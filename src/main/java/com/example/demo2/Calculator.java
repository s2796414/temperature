package com.example.demo2;

public class Calculator {

    public double celsiusToFahrenheit(double celsius) {
        return ((celsius * 1.8) + 32);
    }

}
